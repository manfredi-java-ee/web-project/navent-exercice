## Description

Evaluación técnica navent 2017

Aplicación web ingreso de pedidos

## Questions 

2- Suponiendo que la tabla Pedidos tiene muchos registros, que consideraciones se deberían tener
en cuenta para realizar consultas a la base de datos de manera eficiente?

Para realizar las consultas de manera eficiente se puede evitar el uso excesivo de joins asi como tambien se podria implementar alguna base de datos en memoria como Redis que reduzca el acceso a la base de datos SQL.
Para el caso de las búsquedas podria adhisionarse otro tipo de base de datos como puede ser ELASTICSEARCH donde sean replicados los mismos datos 



## Reference 

Spring 3.0.5.

spring-core
spring-jdbc 
spring-webmvc
mysql
Bootstrap 3
jquery


## Demo 

Server: digital ocean

http://67.205.187.38:8080/navent/pedidos/
  
## License

julian manfredi 2017