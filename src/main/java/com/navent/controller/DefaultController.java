package com.navent.controller;
 

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.navent.domain.model.CustomErrorType;
import com.navent.domain.model.Pedido;
import com.navent.services.ServicePedido;




@Controller
@RequestMapping("/pedidos")
public class DefaultController {
	
	@Autowired
   	ServicePedido servicePedido ;
	
	
	@RequestMapping(value="/", method = RequestMethod.GET)
	public String getIndex() {
	 	
		return "index";
	}
	
		
	@RequestMapping(value="/test/{name}", method = RequestMethod.GET)
	public String getTest(@PathVariable String name, ModelMap model) {

		model.addAttribute("pregunta", name);
		model.addAttribute("respuesta", "prueba ");
		
		return "list";
	}

	
	
	
	@RequestMapping(value = "guardar" ,method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?>  create(@RequestBody  Pedido param) {
		
		   	servicePedido.generarPedido(param);
		    
		    return new ResponseEntity<Pedido>(param, HttpStatus.OK);	 
	}
	
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "grabar/{id}", method = RequestMethod.PUT )
	@ResponseBody
	public  ResponseEntity<?> update(@PathVariable("id") final Integer id , @RequestBody Pedido param) {
		
		
		Pedido pedidoCurrent = servicePedido.buscarPorId(id);
	
 		   if (pedidoCurrent == null) {
 			   
 			    return new ResponseEntity(new CustomErrorType("Unable to upate. pedido with id " + id + " not found."),
 	                    HttpStatus.NOT_FOUND);
 	        }
 		
 		   	servicePedido.editarPedido(param);
 		    
 		    return new ResponseEntity<Pedido>(param, HttpStatus.OK);	 
 		 
	}
	
	

	
	@RequestMapping(value = "selectAll", method = RequestMethod.GET)
	@ResponseBody
	public  ResponseEntity<List<Pedido>> findAll() {
		
			List<Pedido>  users = servicePedido.buscar();
		
		   if (users.isEmpty()) {
	            return new ResponseEntity(HttpStatus.NO_CONTENT);
	        }
	        return new ResponseEntity<List<Pedido>>(users, HttpStatus.OK);
	}
 
	

	
	
}