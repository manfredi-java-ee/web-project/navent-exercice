package com.navent.cache;

import java.util.HashMap;
import java.util.Map;

 public class BumexMemcached {

 
	private static final  Map<String, Object> cachelist = new HashMap<String, Object>();
	 
	private static BumexMemcached instance = null;
	

	protected BumexMemcached() {
	}

	public static BumexMemcached getInstance() {
		if (instance == null) {
			instance = new BumexMemcached();
		}
		return instance;
	}

	public void set(final String key, final Object value) {
		
		cachelist.put(key, value);
	}

	public Object get(final String key) {
		return cachelist.get(key);
	}

	public void delete(String key) {
		cachelist.remove(key);
	}
	 

	
}
