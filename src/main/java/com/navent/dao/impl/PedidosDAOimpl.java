package com.navent.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.navent.dao.PedidosDAO;
import com.navent.domain.model.Pedido;


@Repository
public class PedidosDAOimpl extends JdbcDaoSupport implements PedidosDAO {

	
	
	@Override
	public void insertOrUpdate(Pedido pedido) {
		
		
		if(this.select(pedido.getIdpedido())!=null)
		{
			
			 getJdbcTemplate().update(
						"UPDATE pedidos SET ( nombre = ?"
						+ "                   monto = ? "
						+ "					  descuento = ? ) "
						+ "                   WHERE idpedido = ?", 
						new Object[] { pedido.getNombre(),
									   pedido.getMonto(),
									   pedido.getDescuento(),
									   pedido.getIdpedido()
						});
		} else {
			 
			getJdbcTemplate().update(
						"INSERT INTO pedidos ( nombre,"
						+ "                     monto,"
						+ "					    descuento )"
						+ "                     VALUES (?, ?, ?)", 
						new Object[] { pedido.getNombre(),
									   pedido.getMonto() ,
									   pedido.getDescuento()
									   
						});
		}
 	}
	
	@Override
	public void delete(Pedido pedido) {
		
		 getJdbcTemplate().update(
					"DELETE FROM pedidos WHERE ipedido = ? ", 
					new Object[] { pedido.getIdpedido()
					});
		
	}

	@Override
	public Pedido select(Integer idPedido) {

		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT *  ");
	    sql.append("FROM  pedidos ");
	    sql.append("WHERE idpedido = ?  ");
	     
		final List<Pedido> list = getJdbcTemplate().query(sql.toString(),
				new String[] { String.valueOf(idPedido) }, new PedidoRowMapper());
		return list.isEmpty() ? null : list.get(0);
		
	}
	
	
	@Override
	public List<Pedido> selectAll() {

		final StringBuilder sql = new StringBuilder();
		sql.append("SELECT *  ");
	    sql.append("FROM  pedidos ");
 	     
		final List<Pedido> list = getJdbcTemplate().query(sql.toString(), new PedidoRowMapper());
		return list.isEmpty() ? null : list;
		
	}
	
	
	private static class PedidoRowMapper implements RowMapper<Pedido> {
		@Override
		public Pedido mapRow(final ResultSet rs, final int rowNum) throws SQLException {
			final Pedido pedidoRow = new Pedido();

			pedidoRow.setIdpedido(rs.getInt("IDPEDIDO"));
			pedidoRow.setNombre(rs.getString("NOMBRE"));
			pedidoRow.setMonto(rs.getInt("MONTO"));
			pedidoRow.setDescuento(rs.getInt("DESCUENTO"));

			return pedidoRow;
		}
		
	}
	
	

}
