package com.navent.dao;

import java.util.List;

import com.navent.domain.model.Pedido;


public interface PedidosDAO {
	
	void insertOrUpdate(Pedido pedido);
	
	void delete(Pedido pedido);
	
	Pedido select(Integer idPedido);
	 
	List<Pedido> selectAll();
}
