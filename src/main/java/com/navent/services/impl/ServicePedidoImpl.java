package com.navent.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.navent.cache.BumexMemcached;
import com.navent.dao.PedidosDAO;
import com.navent.domain.model.Pedido;
import com.navent.services.ServicePedido;

@Service
public class ServicePedidoImpl implements ServicePedido {

	
	private BumexMemcached cache = BumexMemcached.getInstance();

	
	@Autowired
	private PedidosDAO dao;
	
	
	@Override
	public Pedido generarPedido(Pedido pedido) {
		
		this.cache.set(String.valueOf(pedido.getIdpedido()), pedido);
		
		dao.insertOrUpdate(pedido);
		
		return pedido;
	}

	@Override
	public Pedido editarPedido(Pedido pedido) {

		if (this.cache.get(String.valueOf(pedido.getIdpedido())) != null) {
			this.cache.delete(String.valueOf(pedido.getIdpedido()));
			this.cache.set(String.valueOf(pedido.getIdpedido()),pedido);
		}

		dao.insertOrUpdate(pedido);
		return pedido;
	}

		
	@Override
	public Pedido borrarPedido(Pedido pedido) {

		if (this.cache.get(String.valueOf(pedido.getIdpedido())) != null) {
			this.cache.delete(String.valueOf(pedido.getIdpedido()));
		}

		dao.delete(pedido);

		return pedido;
	}
	
		
		
	@Override
	public Pedido buscarPorId(Integer idPedido) {

		Pedido pedidoCache = (Pedido) this.cache.get(String.valueOf(idPedido));

		if (pedidoCache != null) {
			return pedidoCache;
		} else

		{
			return dao.select(idPedido);
		}

	}
		
	
	@Override
	public List<Pedido> buscar() {
 
			return dao.selectAll();
	}
	
}
