package com.navent.services;

import java.util.List;

import com.navent.domain.model.Pedido;

public interface ServicePedido {
	
	
	public  Pedido generarPedido(Pedido pedido);
	
	public  Pedido editarPedido(Pedido pedido);
		
	public  Pedido borrarPedido(Pedido pedido);
	
	public  Pedido buscarPorId(Integer pedido);
	
	public  List<Pedido> buscar() ;

}
