<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ejercicio Navent</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
   
  <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/slider.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/fonts/linearicons/style.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/fonts/flaticon/font/flaticon.css">
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap-select.min.css" type="text/css" rel="stylesheet">
    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="${pageContext.request.contextPath}/resources/css/colors/default.css">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.ico" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="${pageContext.request.contextPath}/resources/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
   
   
     <link href="${pageContext.request.contextPath}/resources/css/ui.jqgrid.css" rel="stylesheet" type="text/css" />
	 <link href="${pageContext.request.contextPath}/resources/css/ui/1.10.4/themes/redmond/jquery-ui-1.10.4.custom.css" rel="stylesheet" 	type="text/css" />
   

   <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script type="text/javascript" src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ie-emulation-modes-warning.js"></script>
</head>
<body>

<!-- Header start -->
<header class="main-header">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Navegación</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="logo">
                    <img src="${pageContext.request.contextPath}/resources/img/logos/logo.png" alt="LOGO">
                </a>
            </div>
   
            <!-- /.navbar-collapse -->
            <!-- /.container -->
        </nav>
    </div>
</header>
<!-- Header end -->
 
<!-- section-3 start-->
<div class="section-3 content-area">
    <div class="container">
        <div class="main-title">
            <h1>Ejercicio navent</h1>
        </div>
        <div class="row">
        <p>Implemente en una pagina HTML con el codigo javascript correspondiente el formulario para
		guardar un Pedido a trav�s de una invocacion AJAX a la URI /pedidos/guardar. Aplicar las
		siguientes validaciones a los campos:</p>
         
                 <p>1 - nombre es un campo obligatorio y no puede superar los 100 caracteres</p>
  				 <p>2 - monto es un campo obligatorio del tipo integer</p>
                 <p>3 - descuento es un campo del tipo integer</p>
                   <div style="overflow: auto;">
				<table id="list-new"></table>
 			</div>
             <div id="resultEnvio" class="alert alert-warning alert-block"></div>
             <button type="button" onclick="$('#plantipo').val('1');" class="btn btn-view" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Generar pedido</button>
         
         
       
        </div>
        
        
        	
    </div>
    
    
</div>
<!-- section-3 end-->



<!-- Footer start-->
<footer class="main-footer clearfix">
    <div class="container">
        <!-- Sub footer-->
        <div class="sub-footer">
            <div class="row">
          
     
            </div>
        </div>
    
    </div>
</footer>
<!-- Footer end-->

<!-- Copy right start-->
<div class="copy-right">
    <div class="container">
        <p>Copyright 2017.</p>
    </div>
</div>
<!-- Copy right end-->
 
<!-- MODAL -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ingreso de pedido</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form  id="formEnviar">
          <div class="form-group">
            <label for="recipient-name" class="form-control-label">Nombre:</label>
            <input type="text" class="form-control" name="nombre"  id="nombre" 
            	  maxlength="100"
            	  data-fv-stringlength-message="El nombre no debe ser mayor a 100 caracteres"
             required>
          </div>
          <div class="form-group">
            <label for="message-text" class="form-control-label">Monto:</label>
            <input class="form-control" type="number" name="monto" id="monto"  required>
          </div>
		   <div class="form-group">
            <label for="message-text" class="form-control-label">Descuento:</label>
            <input class="form-control" type="number" name="descuento" id="descuento"  required>
          </div>
 
     <!-- hidden button		  
	    <input type="hidden" name="plantipo" id="plantipo" value="">
     -->
	<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	<button type="submit" class="btn btn-view">Grabar pedido</button>
	
							
      </div>
        </form>
      </div>
      
    </div>
  </div>
</div>
 
 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap-slider.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/wow.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.scrollUp.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap-select.min.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/ie10-viewport-bug-workaround.js"></script>

<!-- Custom javascript -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/app.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jqgrid/i18n/grid.locale-en.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jqgrid/jquery.jqGrid.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jqgrid/grid.subgrid.js"></script>
   

<script type="text/javascript" charset="utf-8">     
			
$(document).ready(function() {   
	
		
	var url = window.location;

	
	jQuery("#list-new").jqGrid({
	   	url: url + "selectAll",
		datatype: "json",
	   	colNames:['ID','NOMBRE','MONTO','DESCUENTO'],
	   	colModel:[
 	   		{name:'idpedido',index:'idpedido', width:50},
 	  	 	{name:'nombre',index:'nombre', width:100,sortable:false},
	   		{name:'monto',index:'monto', width:100,sortable:false},
	   		{name:'descuento',index:'descuento', width:200,sortable:false}
 	   	 	 ],
 	   	 shrinkToFit:false,
 	   	forceFit:true,
	   	rowNum:10000,
	   	sortname: 'idorder',
	    viewrecords: true,
	  	hidegrid: false,
	    height: "100%",
 
	    gridComplete: function(){
			var ids = jQuery("#list-new").jqGrid('getDataIDs');
 	 
 	    }
		,
		onSelectRow: function() {
			jQuery("#list-new").jqGrid('resetSelection');
		     return false;
		}
		,
		onRightClickRow: function () {
			jQuery("#list-new").jqGrid('resetSelection');
		    return false;
		}
	 	,
	    caption:"PEDIDOS"
	});
	
	formEnviar = $("#formEnviar");
	formEnviar.submit(function(event) {
	           // Stop form from submitting normally
	           event.preventDefault();
	         
	           $("#resultEnvio").html('');
	 	           
	             var nombre = $('#nombre').val();
			     var monto = $('#monto').val();
			     var descuento = $('#descuento').val();
			     
		         var json =  {
					   "nombre": nombre,
					   "monto":monto, 
					   "descuento":descuento
					};
	           
 		         
	           //Send the data using post and put the results in a div 
	           $.ajax({
	               url: url + "guardar" ,
	               type: "POST",
	               data:  JSON.stringify(json),
	         
	               headers: {
	                   'Content-Type' :' application/json',
	                   'Accept': 'application/json'
	                 },
	               timeout:3000 ,
	               success: function(data){
 
		            	$("#resultEnvio").html("Ingreso exitoso");
		            	jQuery("#list-new").trigger("reloadGrid");
						$('#exampleModal').modal('hide'); 
						
						
					    $('#nombre').val("");
						$('#monto').val("");
						$('#descuento').val("");
	               }
	               ,
	               error:function(){
	                   alert("failure");
	                 //  $("#result").html('There is error while submit');
	               }
	           });
	        });

	 
	    
	});

	  
	</script>

</body>
</html>